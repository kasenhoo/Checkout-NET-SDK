using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Collections.Generic;
using PayPalHttp;
using Xunit;
using PayPalCheckoutSdk.Test;
using System.Threading.Tasks;
using PayPalCheckoutSdk.Webhooks;

namespace PayPalCheckoutSdk.Webhooks.Test
{
    [Collection("Webhooks")]
    public class AvailableEventTypesTest
    {
        public static async Task<HttpResponse> getAvailableEventTypes() {
            AvailableEventTypeListRequest request = new AvailableEventTypeListRequest();
            return await TestHarness.client().Execute(request);
        }

        [Fact]
        public async void TestAvailableEventTypeListRequest()
        {
            HttpResponse response = await getAvailableEventTypes();
            Assert.Equal(200, (int) response.StatusCode);
            Assert.NotNull(response.Result<EventTypeList>());
        }
    }
}
