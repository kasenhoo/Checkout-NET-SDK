using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Collections.Generic;
using PayPalHttp;
using Xunit;
using PayPalCheckoutSdk.Test;
using static PayPalCheckoutSdk.Test.TestHarness;
using Xunit.Abstractions;
using Test;


namespace PayPalCheckoutSdk.Webhooks.Test
{
    [Collection("Webhooks")]
    public class AvailableEventTypeListTest : BaseTest
    {
        public AvailableEventTypeListTest(ITestOutputHelper a) : base(a)
        {
        }


        [Fact]
        public async void TestAvailableEventTypeListRequest()
        {
            AvailableEventTypeListRequest request = new AvailableEventTypeListRequest();

            HttpResponse response = await TestHarness.client().Execute(request);
            Assert.Equal(200, (int)response.StatusCode);
            Assert.NotNull(response.Result<EventTypeList>());
            ito.WriteLine(response.ToString());
            // Add your own checks here
        }
    }
}
