using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Collections.Generic;
using PayPalHttp;
using Xunit;
 
using System.Threading.Tasks;
using PayPalCheckoutSdk.Webhooks;
using PayPalCheckoutSdk.Test;

namespace PayPalCheckoutSdk.Webhooks.Test
{
    [Collection("Webhooks")]
    public class WebhookListTest
    {

        public static async Task<HttpResponse>  GetAllWebhooks()
        {
            WebhookListRequest request = new WebhookListRequest();
            return await TestHarness.client().Execute(request);
        }

        [Fact]
        public async void TestWebhookListRequest()
        {
            HttpResponse createResponse = await WebhookCreateTest.createWebhook();
            var response = await GetAllWebhooks();
            Assert.Equal(200, (int) response.StatusCode);
            Assert.NotNull(response.Result<WebhookList>());
        }
    }
}
