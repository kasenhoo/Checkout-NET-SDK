using System;
using PayPalCheckoutSdk.Core;
using PayPalHttp;
using Xunit;
[assembly: CollectionBehavior(MaxParallelThreads = -1)]

namespace PayPalCheckoutSdk.Test
{
    public class TestHarness
    {

        public static PayPalEnvironment environment()
        {
            return new LiveEnvironment(
                System.Environment.GetEnvironmentVariable("PAYPAL_CLIENT_ID") != null ?
                 System.Environment.GetEnvironmentVariable("PAYPAL_CLIENT_ID") : "AeE_oLWbjnc-Y_CMRXYLGkAZzHP3_-WnIuSBR7S9FP-xnxGwLPf7pJLZpp2xa1W4tGU8X0N8YOk-YNK7",
                System.Environment.GetEnvironmentVariable("PAYPAL_CLIENT_SECRET") != null ?
                 System.Environment.GetEnvironmentVariable("PAYPAL_CLIENT_SECRET") : "EHjJbBrkmCx0CX1wgr__jORKjLqx24po0RK89GDLs2E2MNozVbVpz__HDGDKDXEGLBtpO4T6DHMQAf_-");
        }

        public static HttpClient client()
        {
            return new PayPalHttpClient(environment());
        }

        public static HttpClient client(string refreshToken)
        {
            return new PayPalHttpClient(environment(), refreshToken);
        }
    }
}
